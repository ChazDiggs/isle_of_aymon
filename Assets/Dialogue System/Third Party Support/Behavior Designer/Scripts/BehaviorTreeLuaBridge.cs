using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using PixelCrushers.DialogueSystem.SequencerCommands;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace PixelCrushers.DialogueSystem.BehaviorDesigner {
	
	/// <summary>
	/// This script synchronizes a behavior tree's shared variables with the
	/// Dialogue System's Lua environment. Attach it to the GameObject that
	/// contains the behavior tree. Synchronization occurs automatically at
	/// the beginning and end of conversations. You can also synchronize manually
	/// by calling SyncToLua() or SyncFromLua().
	/// 
	/// The Lua variables will have the name <em>gameObjectName_variableName</em>.
	/// All blank spaces and hyphens will be converted to underscores.
	/// For example, say an NPC named Private Hart has a behavior tree with a shared
	/// variable named Angry. The Lua variable will be <c>Variable["Private_Hart_Angry"]</c>.
	/// 
	/// Only bools, floats, ints, and strings are synchronized.
	/// </summary>
	[AddComponentMenu("Dialogue System/Third Party/Behavior Designer/Behavior Tree Lua Bridge")]
	public class BehaviorTreeLuaBridge : MonoBehaviour {
		
		// The behavior tree on this GameObject:
		private Behavior behavior = null;
		
		void Awake() {
			behavior = GetComponentInChildren<Behavior>();
		}
		
		/// <summary>
		/// When a conversation starts, sync to Lua. This makes the behavior
		/// tree data available to conversations in Conditions and User Scripts.
		/// </summary>
		/// <param name="actor">The other actor.</param>
		public void OnConversationStart(Transform actor) {
			SyncToLua();
		}
		
		/// <summary>
		/// When a conversation ends, sync from Lua back into the behavior
		/// tree. If the conversation has changed any values, the changes
		/// will be reflected in the behavior tree.
		/// </summary>
		/// <param name="actor">The other actor.</param>
		public void OnConversationEnd(Transform actor) {
			SyncFromLua();
		}
		
		/// <summary>
		/// Syncs the behavior tree's shared variables to the Dialogue System's
		/// Lua environment. 
		/// </summary>
		public void SyncToLua() {
			if (behavior == null) return;
			foreach (SharedVariable variable in behavior.GetBehaviorSource().Variables) {
				if (IsSyncableType(variable)) {
					DialogueLua.SetVariable(GetLuaVariableName(variable.Name), variable.GetValue());
				}
			}
		}
		
		/// <summary>
		/// Syncs the Dialogue System's Lua environment back into the behavior tree's 
		/// shared variables.
		/// </summary>
		public void SyncFromLua() {
			if (behavior == null) return;
			foreach (SharedVariable variable in behavior.GetBehaviorSource().Variables) {
				if (IsSyncableType(variable)) {
					Lua.Result result = DialogueLua.GetVariable(GetLuaVariableName(variable.Name));
					if (result.HasReturnValue) {
						variable.SetValue(CastLuaResult(variable, result));
					}
				}
			}
		}
		
		private bool IsSyncableType(SharedVariable variable) {
			return (variable is SharedBool) ||
				(variable is SharedFloat) ||
					(variable is SharedInt) ||
					(variable is SharedString);
		}
		
		private string GetLuaVariableName(string variableName) {
			return string.Format("{0}_{1}", name, variableName);
		}
		
		private object CastLuaResult(SharedVariable variable, Lua.Result result) {
			if (variable is SharedBool) return result.AsBool;
			if (variable is SharedFloat) return result.AsFloat;
			if (variable is SharedInt) return result.AsInt;
			if (variable is SharedString) return result.AsString;
			return null;
		}
		
	}
	
}