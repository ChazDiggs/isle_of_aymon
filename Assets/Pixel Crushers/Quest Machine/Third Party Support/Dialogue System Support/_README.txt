﻿/*
Quest Machine Dialogue System Integration

To enable Dialogue System integration, you must import two packages:

- Plugins / Pixel Crushers / Common / Third Party Support / Dialogue System Support.unitypackage
- Plugins / Pixel Crushers / Quest Machine / Third Party Support / Dialogue System Support.unitypackage

*/