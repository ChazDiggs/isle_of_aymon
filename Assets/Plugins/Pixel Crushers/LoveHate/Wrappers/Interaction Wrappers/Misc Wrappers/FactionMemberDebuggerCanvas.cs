﻿// Copyright © Pixel Crushers. All rights reserved.

using UnityEngine;

namespace PixelCrushers.LoveHate.Wrappers
{

    /// <summary>
    /// This wrapper keeps references intact if you switch between the compiled
    /// assembly and source code versions of the original class.
    /// </summary>
    [AddComponentMenu("Love\u2215Hate/Debug/Faction Member Debugger Canvas")]
    public class FactionMemberDebuggerCanvas : PixelCrushers.LoveHate.FactionMemberDebuggerCanvas
    {
    }

}
